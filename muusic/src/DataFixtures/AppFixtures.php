<?php

namespace App\DataFixtures;

use App\Entity\Style;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        for ($i=1; $i < 20; $i++) { 
            $style = new Style();
            $style->setName('name ' . $i)
               ->setStatut('false')
                ->setDate('test'. $i)
            ->setDescription('test ' . $i);
            

            $manager->persist($style);
        }

        $manager->flush();
    }
}
