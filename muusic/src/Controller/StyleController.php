<?php

namespace App\Controller;

use Symfony\Component\Form\FormTypeInterface;
use App\Entity\Style;
use App\Repository\StyleRepository as RepositoryStyleRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


final class StyleController extends AbstractController
{
    #[Route('/style', name: 'style', methods:['GET', 'POST'])]
    public function index(RepositoryStyleRepository $repository): Response
    {
        $style = $repository->findAll();
        return $this->render('home.html.twig', [
            'style' => $style
        ]);
    }

    #[Route('/style/nouveau', 'style.new', methods: ['GET', 'POST'])]
    public function new(
        Request $request,
        EntityManagerInterface $manager
    ): Response {
        $style = new Style();
        $form = $this->createForm(Style::class, $style);

        dd($request);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $style = $form->getData();

            dd($style);

            $manager->persist($style);
            $manager->flush();
        }

        return $this->render('style/new.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
