<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Document</title>
</head>
<body>
    <?php 


    ?>

<h2>Ajouter une nouvelle tâche :</h2>
<form method="POST" action="traitement.php">
  <label for="titre">Titre :</label>
  <input type="text" name="titre" id="titre" required>

  <label for="description">Description :</label>
  <textarea name="description" id="description" required></textarea>

  <label for="date_de_creation">Date de création :</label>
  <input type="date" name="date_de_creation" id="date_de_creation" required>

  <label for="statut">Statut :</label>
  <select name="statut" id="statut" required>
    <option value="en cours">En cours</option>
    <option value="terminée">Terminée</option>
    <option value="annulée">Annulée</option>
  </select>

  <label for="nom">Nom :</label>
  <input type="text" name="nom" id="nom" required>

  <label for="classe">Classe :</label>
  <input type="text" name="classe" id="classe">

  <button type="submit">Ajouter</button>
</form>



</body>
</html>