<?php

require 'connexion.php';

try {
    $pdo = new PDO("mysql:host=$host;dbname=$dbName;charset=utf8", $user, $password);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    echo "Erreur de connexion à la base de données : " . $e->getMessage();
    // Gérer l'erreur de connexion à la base de données
}

// Récupérer toutes les tâches
$stmtTaches = $pdo->query("SELECT * FROM tache");
$taches = $stmtTaches->fetchAll(PDO::FETCH_ASSOC);

// Récupérer toutes les classes
$stmtClasses = $pdo->query("SELECT * FROM classe");
$classes = $stmtClasses->fetchAll(PDO::FETCH_ASSOC);
?>

<!DOCTYPE html>
<html>
<head>
<title>Liste des tâches et classes</title>
<style>
    table {
        border-collapse: collapse;
    }
    th, td {
        border: 1px solid black;
        padding: 5px;
    }
</style>
</head>
<body>
<h1>Liste des tâches</h1>
<table>
    <tr>
        <th>ID</th>
        <th>Titre</th>
        <th>Description</th>
        <th>Date de création</th>
        <th>Statut</th>
    </tr>
    <?php foreach ($taches as $tache): ?>
        <tr>
            <td><?= $tache['id'] ?></td>
            <td><?= $tache['titre'] ?></td>
            <td><?= $tache['description'] ?></td>
            <td><?= $tache['date_de_creation'] ?></td>
            <td><?= $tache['statut'] ?></td>
        </tr>
    <?php endforeach; ?>
</table>

<h1>Liste des classes</h1>
<table>
    <tr>
        <th>ID</th>
        <th>Nom</th>
        <th>Description</th>
    </tr>
    <?php foreach ($classes as $classe): ?>
        <tr>
            <td><?= $classe['id'] ?></td>
            <td><?= $classe['nom'] ?></td>
            <td><?= $classe['description'] ?></td>
        </tr>
    <?php endforeach; ?>
</table>
</body>
</html>
