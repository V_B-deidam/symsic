<?php

class Tache {

    private $titre;
    private $description;
    private $date_de_creation;
    private $statut;

    public function __construct( $titre, $description, $date_de_creation, $statut) {

        $this->titre = $titre;
        $this->description = $description;
        $this->date_de_creation = $date_de_creation;
        $this->statut = $statut;
    }

    public function getTitre() {
        return $this->titre;
    }

    public function setTitre($titre) {
        $this->titre = $titre;
    }

    public function getDescription() {
        return $this->description;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function getDateDeCreation() {
        return $this->date_de_creation;
    }

    public function setDateDeCreation($date_de_creation){
        return $this->date_de_creation = $date_de_creation;
    }

    public function getStatut() {
        return $this->statut;
    }

    public function setStatut($statut) {
        $this->statut = $statut;
    }

    // public function getClasse(){
    //     return $this->classe;
    // }

    // public function setClasse($classe) {
    //     $this->classe = $classe;
    // }
}