<?php
require 'connexion.php';

try {
    $pdo = new PDO("mysql:host=$host;dbname=$dbName;charset=utf8", $user, $password);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    echo "Erreur de connexion à la base de données : " . $e->getMessage();
    exit();
}

// Récupérer les tâches depuis la base de données
$stmt = $pdo->query("SELECT t.id AS tache_id, t.titre, t.description, t.date_de_creation, t.statut, c.nom
                     FROM tache AS t
                     LEFT JOIN classe AS c ON t.id = c.tache_id");
$taches = $stmt->fetchAll(PDO::FETCH_ASSOC);

// Récupérer les classes depuis la base de données
$stmt = $pdo->query("SELECT id, nom, description FROM classe");
$classes = $stmt->fetchAll(PDO::FETCH_ASSOC);

// Traitement du formulaire d'ajout de tâche
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (isset($_POST['titre'], $_POST['description'], $_POST['date_de_creation'], $_POST['statut'], $_POST['tache_id'])) {
        $titre = $_POST['titre'];
        $description = $_POST['description'];
        $date_de_creation = $_POST['date_de_creation'];
        $statut = $_POST['statut'];
        $tache_id = $_POST['classe'];
        $nom = $_POST['nom'];

        // Insérer la nouvelle tâche en base de données
        $stmt = $pdo->prepare("INSERT INTO tache (titre, description, date_de_creation, statut) VALUES (?, ?, ?, ?)");
        $stmt->execute([$titre, $description, $date_de_creation, $statut]);

        // Associer la tâche à la classe sélectionnée
        $classe_id = $tache_id;
        $stmt = $pdo->prepare("INSERT INTO classe (nom, description, tache_id) VALUES (?, ?, ?)");
        $stmt->execute([$nom, $description, $classe_id]);

        // Rediriger après l'ajout de la tâche
        header("Location: table.php");
        exit();
    }
}
?>

<!DOCTYPE html>
<html>

<head>
    <title>Liste des tâches et classes</title>
    <style>
        table {
            border-collapse: collapse;
        }

        th,
        td {
            border: 1px solid black;
            padding: 5px;
        }
    </style>
</head>

<body>
    <h1>Liste des tâches et classes</h1>
    <table>
        <tr>
            <th>ID</th>
            <th>Titre</th>
            <th>Description</th>
            <th>Date de création</th>
            <th>Statut</th>
            <th>Classe</th>
            <th>nom</th>
            <th>Actions</th>
        </tr>
        <?php foreach ($taches as $tache) : ?>
            <tr>
                <td><?= $tache['tache_id'] ?></td>
                <td><?= $tache['titre'] ?></td>
                <td><?= $tache['description'] ?></td>
                <td><?= $tache['date_de_creation'] ?></td>
                <td><?= $tache['statut'] ?></td>
                <td>
                    <form method="POST" action="">
                        <input type="hidden" name="tache_id" value="<?= $tache['tache_id'] ?>">
                        <select name="nouveau_statut" onchange="this.form.submit()">
                            <option value="En cours" <?= $tache['statut'] === 'En cours' ? 'selected' : '' ?>>En cours</option>
                            <option value="Terminé" <?= $tache['statut'] === 'Terminé' ? 'selected' : '' ?>>Terminé</option>
                            <option value="Terminé" <?= $tache['statut'] === 'Terminé' ? 'selected' : '' ?>>Annulée</option>
                        </select>
                    </form>
                </td>
                <td>
                    <select id="classe" name="classe" required>
                        <?php foreach ($classes as $classe) : ?>
                            <option value="<?= $classe['id'] ?>"><?= $classe['nom'] ?></option>
                        <?php endforeach; ?>
                    </select>
                </td>
                <td>
                    <form method="POST" action="">
                        <input type="hidden" name="tache_id" value="<?= $tache['tache_id'] ?>">
                        <button type="submit" name="action" value="supprimer">Supprimer</button>
                    </form>
                </td>
            </tr>
        <?php endforeach; ?>
    </table>

    <h2>Ajouter une nouvelle tâche</h2>
    <form method="POST" action="">
        <label for="titre">Titre :</label>
        <input type="text" id="titre" name="titre" required><br>

        <label for="description">Description :</label>
        <textarea id="description" name="description" required></textarea><br>

        <label for="date_de_creation">Date de création :</label>
        <input type="date" id="date_de_creation" name="date_de_creation" required><br>

        <label for="statut">Statut :</label>
        <select id="statut" name="statut" required>
            <option value="En cours">En cours</option>
            <option value="Terminé">Terminé</option>
            <option value="Terminé">Annulée</option>
        </select><br>

        <label for="nom">nom :</label>
        <input type="text" name="nom" id="nom"><br>

        <button type="submit">Ajouter</button>
    </form>
</body>

</html>