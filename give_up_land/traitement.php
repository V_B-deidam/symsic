<?php
    require  'POO/classe.php';   
    require  'POO/tache.php';
    require  'connexion.php';

    try {
        $pdo = new PDO("mysql:host=$host;dbname=$dbName;charset=utf8", $user, $password);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    } catch (PDOException $e) {
        echo "Erreur de connexion à la base de données : " . $e->getMessage();
        // Gérer l'erreur de connexion à la base de données
    }
    
    // Code pour traiter le formulaire
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        // Récupérer les données du formulaire
        $titre = $_POST['titre'];
        $description = $_POST['description'];
        $date_de_creation = $_POST['date_de_creation'];
        $statut = $_POST['statut'];
        $nom = $_POST['nom'];
        $classe = $_POST['classe'];
    
        // Créer une nouvelle instance de Tache
        $nouvelleTache = new Tache($titre, $description, $date_de_creation, $statut);
        $nouvelleTache->setTitre($titre);
        $nouvelleTache->setDescription($description);
        $nouvelleTache->setDatedeCreation($date_de_creation);
        $nouvelleTache->setStatut($statut);
    
        // Récupérer l'entité Classe correspondante
        if (!empty($nom)) {
            // Créer une nouvelle instance de Classe
            $nouvelleClasse = new Classe($nom, $description);
            $nouvelleClasse->setNom($nom);
        }
       
        
        // Insérer la nouvelle tâche en base de données
        $stmt = $pdo->prepare("INSERT INTO tache (titre, description, date_de_creation, statut) VALUES (?, ?, ?, ?)");
        $stmt1 = $pdo->prepare("INSERT INTO classe (nom, description) VALUES (?, ?)");
        $stmt->execute([$nouvelleTache->getTitre(), $nouvelleTache->getDescription(), $nouvelleTache->getDatedeCreation(), $nouvelleTache->getStatut()]);
        $stmt1->execute([$nouvelleClasse->getNom(), $nouvelleClasse->getDescription()]);

        // Rediriger ou afficher un message de succès
        // Code pour rediriger ou afficher un message de succès
        // if (isset($nouvelleClasse)) {
        //     $stmt1->execute([$nouvelleClasse->getNom(), $nouvelleClasse->getDescription()]);
        // }
    }
    









