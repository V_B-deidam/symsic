<?php
require 'connexion.php';

try {
    $pdo = new PDO("mysql:host=$host;dbname=$dbName;charset=utf8", $user, $password);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    echo "Erreur de connexion à la base de données : " . $e->getMessage();
    // Gérer l'erreur de connexion à la base de données
}

// Récupérer toutes les tâches
$stmtTaches = $pdo->query("SELECT * FROM tache");
$taches = $stmtTaches->fetchAll(PDO::FETCH_ASSOC);

// Récupérer toutes les classes
$stmtClasses = $pdo->query("SELECT * FROM classe");
$classes = $stmtClasses->fetchAll(PDO::FETCH_ASSOC);

// Traitement du formulaire pour ajouter une nouvelle tâche
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // Vérifier si les données du formulaire ont été soumises
    if (isset($_POST['titre']) && isset($_POST['description']) && isset($_POST['date_de_creation']) && isset($_POST['statut'])) {
        $titre = $_POST['titre'];
        $description = $_POST['description'];
        $date_de_creation = $_POST['date_de_creation'];
        $statut = $_POST['statut'];
        

        // Insérer la nouvelle tâche en base de données
        $stmtInsert = $pdo->prepare("INSERT INTO tache (titre, description, date_de_creation, statut) VALUES (?, ?, ?, ?)");
        $stmtInsert->execute([$titre, $description, $date_de_creation, $statut]);

        // Rediriger vers la page de liste des tâches
        header("Location: liste.php");
        exit();
    }
}

// Traitement pour mettre à jour le statut d'une tâche
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // Vérifier si les données du formulaire de mise à jour ont été soumises
    if (isset($_POST['tache_id']) && isset($_POST['nouveau_statut'])) {
        $tache_id = $_POST['tache_id'];
        $nouveau_statut = $_POST['nouveau_statut'];

        // Mettre à jour le statut de la tâche en base de données
        $stmtUpdate = $pdo->prepare("UPDATE tache SET statut = ? WHERE id = ?");
        $stmtUpdate->execute([$nouveau_statut, $tache_id]);

        // Rediriger vers la page de liste des tâches
        header("Location: script.php");
        exit();
    }
}

// Traitement pour supprimer une tâche
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // Vérifier si l'ID de la tâche à supprimer a été soumis
    if (isset($_POST['supprimer_tache_id'])) {
        $tache_id = $_POST['supprimer_tache_id'];

        // Supprimer la tâche de la base de données
        $stmtDelete = $pdo->prepare("DELETE FROM tache WHERE id = ?");
        $stmtDelete->execute([$tache_id]);

        // Rediriger vers la page de liste des tâches
        header("Location: table.php");
        exit();
    }
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Liste des tâches et classes</title>
    <style>
        table {
            border-collapse: collapse;
        }
        th, td {
            border: 1px solid black;
            padding: 5px;
        }
    </style>
</head>
<body>
    <h1>Liste des tâches</h1>
    <table>
        <tr>
            <th>ID</th>
            <th>Titre</th>
            <th>Description</th>
            <th>Date de création</th>
            <th>Statut</th>
            <!-- <th>Classe</th> -->
            <th>Actions</th>
        </tr>
        <?php foreach ($taches as $tache): ?>
            <tr>
                <td><?= $tache['id'] ?></td>
                <td><?= $tache['titre'] ?></td>
                <td><?= $tache['description'] ?></td>
                <td><?= $tache['date_de_creation'] ?></td>
                <td>
                    <form method="POST" action="">
                        <input type="hidden" name="tache_id" value="<?= $tache['id'] ?>">
                        <select name="nouveau_statut" onchange="this.form.submit()">
                            <option value="En cours" <?= $tache['statut'] === 'En cours' ? 'selected' : '' ?>>En cours</option>
                            <option value="Terminée" <?= $tache['statut'] === 'Terminée' ? 'selected' : '' ?>>Terminée</option>
                            <option value="En cours" <?= $tache['statut'] === 'En cours' ? 'selected' : '' ?>>Annulée</option>
                        </select>
                    </form>
                </td>
                <!-- <td><?= $tache['id'] ?></td> -->
                <td>
                    <form method="POST" action="">
                        <input type="hidden" name="supprimer_tache_id" value="<?= $tache['id'] ?>">
                        <button type="submit">Supprimer</button>
                    </form>
                </td>
            </tr>
        <?php endforeach; ?>
    </table>

    <h1>Liste des classes</h1>
    <table>
        <tr>
            <th>ID</th>
            <th>Nom</th>
            <th>Description</th>
            <th>Tache_id</th>
        </tr>
        <?php foreach ($classes as $classe): ?>
            <tr>
                <td><?= $classe['id'] ?></td>
                <td><?= $classe['nom'] ?></td>
                <td><?= $classe['description'] ?></td>
                <td><?= $classe['tache_id'] ?></td>
            </tr>
        <?php endforeach; ?>
    </table>

    <h1>Ajouter une nouvelle tâche</h1>
    <form method="POST" action="">
        <label for="titre">Titre :</label>
        <input type="text" name="titre" id="titre" required><br>

        <label for="description">Description :</label>
        <input type="text" name="description" id="description" required><br>

        <label for="date_de_creation">Date de création :</label>
        <input type="date" name="date_de_creation" id="date_de_creation" required><br>

        <label for="statut">Statut :</label>
        <select name="statut" id="statut" required>
            <option value="En cours">En cours</option>
            <option value="Terminée">Terminée</option>
            <option value="Terminée">Annulée</option>
        </select><br>

        <label for="classe_id">Classe :</label>
        <select name="classe_id" id="classe_id" required>
            <?php foreach ($classes as $classe): ?>
                <option value="<?= $classe['id'] ?>"><?= $classe['nom'] ?></option>
            <?php endforeach; ?>
        </select><br>

        <button type="submit">Ajouter</button>
    </form>
</body>
</html>
