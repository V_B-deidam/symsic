<?php
class Education extends Utilisateur
{
    private $etablissement;
    private $diplome;
    private $domaine;
    private $dateDebut;
    private $dateFin;
    private $enCours;
    

    public function __construct($etablissement, $diplome, $domaine, $dateDebut, $dateFin, $enCours)
    {
        $this->etablissement = $etablissement;
        $this->diplome = $diplome;
        $this->domaine = $domaine;
        $this->dateDebut = $dateDebut;
        $this->dateFin = $dateFin;
        $this->enCours = $enCours;
    }

    public function getEtablissement()
    {
        return $this->etablissement;
    }
    public function setEtablissement($etablissement)
    {
        $this->etablissement = $etablissement;
    }

    public function getDiplome()
    {
        return $this->diplome;
    }
    public function setDiplome($diplome)
    {
        $this->diplome = $diplome;
    }

    public function getDomaine()
    {
        return $this->domaine;
    }
    public function setDomaine($domaine)
    {
        $this->domaine = $domaine;
    }

    public function getDateDebut()
    {
        return $this->dateDebut;
    }
    public function setDateDebut($dateDebut)
    {
        $this->dateDebut = $dateDebut;
    }

    public function getDateFin()
    {
        return $this->dateFin;
    }
    public function setDateFin($dateFin)
    {
        $this->dateFin = $dateFin;
    }

    public function getEnCours()
    {
        return $this->enCours;
    }
    public function setEnCours($enCours)
    {
        $this->enCours = $enCours;
    }

    public function enregistrerEducation($conn) {
        // Récupérer les propriétés de l'éducation
        $etablissement = $this->getEtablissement();
        $diplome = $this->getDiplome();
        $domaine = $this->getDomaine();
        $dateDebut = $this->getDateDebut();
        $dateFin = $this->getDateFin();
        $enCours = $this->getEnCours();
    
        // Échapper les variables du formulaire pour éviter les injections SQL
        $etablissement = mysqli_real_escape_string($conn, $etablissement);
        $diplome = mysqli_real_escape_string($conn, $diplome);
        $domaine = mysqli_real_escape_string($conn, $domaine);
        $dateDebut = mysqli_real_escape_string($conn, $dateDebut);
        $dateFin = mysqli_real_escape_string($conn, $dateFin);
        $enCours = mysqli_real_escape_string($conn, $enCours);
    
        // Requête SQL pour insérer l'éducation dans la table 'education'
        $sql = "INSERT INTO education (etablissement, diplome, domaine, date_debut, date_fin, en_cours) VALUES ('$etablissement', '$diplome', '$domaine', '$dateDebut', '$dateFin', '$enCours')";
    
        // Exécuter la requête SQL
        $result = $conn->query($sql);
    
        if (!$result) {
            echo "Erreur lors de l'insertion dans la table 'education': " . $conn->error;
        }
    }
}

