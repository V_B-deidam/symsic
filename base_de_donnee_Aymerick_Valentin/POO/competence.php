<?php

class Competence extends Utilisateur
{
    private $competence;

    public function __construct($competence)
    {
        $this->competence = $competence;
    }

    public function getCompetence()
    {
        return $this->competence;
    }

    public function setCompetence($competence)
    {
        $this->competence = $competence;
    }

    public function competenceNom(){
        
    }

    public function enregistrerCompetence($conn)
    {
        $competence = $this->getCompetence();

        // Requête préparée pour insérer la compétence dans la table 'competence'
        $sql = "INSERT INTO competence (competence) VALUES (?)";

        // Préparation de la requête
        $stmt = $conn->prepare($sql);
        $stmt->bind_param("s", $competence);

        // Exécution de la requête
        $result = $stmt->execute();

        if (!$result) {
            echo "Erreur lors de l'insertion dans la table 'competence': " . $conn->error;
        }

        // Fermeture du statement
        $stmt->close();
    }
}
