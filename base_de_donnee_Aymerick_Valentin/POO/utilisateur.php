<?php

class Utilisateur {
    protected $id;
    protected $nom;
    protected $prenom;
    protected $email;
    protected $motDePasse;
    protected $dateDeNaissance;
    protected $genre;
    protected $adresse;
    protected $telephone;
    
    public function __construct($nom, $prenom, $email, $motDePasse, $dateDeNaissance, $genre, $adresse, $telephone) {
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->email = $email;
        $this->motDePasse = $motDePasse;
        $this->dateDeNaissance = $dateDeNaissance;
        $this->genre = $genre;
        $this->adresse = $adresse;
        $this->telephone = $telephone;
    }

    public function getId() {
        return $this->id;
    }
    public function setId($id) {
        $this->id = $id;
    }

    public function getNom() {
        return $this->nom;
    }
    public function setNom($nom) {
        $this->nom = $nom;
    }

    public function getPrenom() {
        return $this->prenom;
    }
    public function setPrenom($prenom) {
        $this->prenom = $prenom;
    }

    public function getEmail() {
        return $this->email;
    }
    public function setEmail($email) {
        $this->email = $email;
    }

    public function getMotDePasse() {
        return $this->motDePasse;
    }
    public function setMotDePasse($motDePasse) {
        $this->motDePasse = $motDePasse;
    }

    public function getDateDeNaissance() {
        return $this->dateDeNaissance;
    }
    public function setDateDeNaissance($dateDeNaissance) {
        $this->dateDeNaissance = $dateDeNaissance;
    }

    public function getGenre() {
        return $this->genre;
    }
    public function setGenre($genre) {
        $this->genre = $genre;
    }

    public function getAdresse() {
        return $this->adresse;
    }
    public function setAdresse($adresse) {
        $this->adresse = $adresse;
    }

    public function getTelephone() {
        return $this->telephone;
    }
    public function setTelephone($telephone) {
        $this->telephone = $telephone;
    }
    
    public function enregistrerUtilisateur($conn) {
        // Récupérer les propriétés de l'utilisateur
        $nom = $this->getNom();
        $prenom = $this->getPrenom();
        $email = $this->getEmail();
        $motDePasse = $this->getMotDePasse();
        $dateDeNaissance = $this->getDateDeNaissance();
        $genre = $this->getGenre();
        $adresse = $this->getAdresse();
        $telephone = $this->getTelephone();
    
        // Échapper les variables du formulaire pour éviter les injections SQL
        $nom = mysqli_real_escape_string($conn, $nom);
        $prenom = mysqli_real_escape_string($conn, $prenom);
        $email = mysqli_real_escape_string($conn, $email);
        $motDePasse = mysqli_real_escape_string($conn, $motDePasse);
        $dateDeNaissance = mysqli_real_escape_string($conn, $dateDeNaissance);
        $genre = mysqli_real_escape_string($conn, $genre);
        $adresse = mysqli_real_escape_string($conn, $adresse);
        $telephone = mysqli_real_escape_string($conn, $telephone);
    
        // Requête SQL pour insérer l'utilisateur dans la table 'utilisateur'
        $sql = "INSERT INTO utilisateur (nom, prenom, email, mot_de_passe, date_de_naissance, genre, adresse, telephone) VALUES ('$nom', '$prenom', '$email', '$motDePasse', '$dateDeNaissance', '$genre', '$adresse', '$telephone')";
    
        // Exécuter la requête SQL
        $result = $conn->query($sql);
    
        if (!$result) {
            echo "Erreur lors de l'insertion dans la table 'utilisateur': " . $conn->error;
        }
    }
}