<?php

$servername = "localhost";
$username = "root";
$password = "AzErTyUiOp";
$dbname = "exo_poo_formu";

// Établir une connexion à la base de données
$conn = new mysqli($servername, $username, $password, $dbname);

// Vérifier la connexion
if (!$conn) {
    die('Erreur de connexion à la base de données : ' . mysqli_connect_error());
}

// Votre code existant pour la connexion à la base de données

// Code pour supprimer la contrainte de clé étrangère existante pour la table education
$sql = "ALTER TABLE education DROP FOREIGN KEY fk_education_utilisateur";
if ($conn->query($sql) === TRUE) {
    echo "Contrainte de clé étrangère supprimée avec succès pour la table education.";
} else {
    echo "Erreur lors de la suppression de la contrainte de clé étrangère pour la table education : " . $conn->error;
}

// Code pour supprimer la contrainte de clé étrangère existante pour la table competence
$sql = "ALTER TABLE competence DROP FOREIGN KEY fk_competence_utilisateur";
if ($conn->query($sql) === TRUE) {
    echo "Contrainte de clé étrangère supprimée avec succès pour la table competence.";
} else {
    echo "Erreur lors de la suppression de la contrainte de clé étrangère pour la table competence : " . $conn->error;
}

// Code pour créer la nouvelle liaison entre la table education et utilisateur
$sql = "ALTER TABLE education ADD CONSTRAINT fk_education_utilisateur FOREIGN KEY (id_utilisateur) REFERENCES utilisateur(id)";
if ($conn->query($sql) === TRUE) {
    echo "Nouvelle contrainte de clé étrangère ajoutée avec succès pour la table education.";
} else {
    echo "Erreur lors de l'ajout de la nouvelle contrainte de clé étrangère pour la table education : " . $conn->error;
}

// Code pour créer la nouvelle liaison entre la table competence et utilisateur
$sql = "ALTER TABLE competence ADD CONSTRAINT fk_competence_utilisateur FOREIGN KEY (id_utilisateur) REFERENCES utilisateur(id)";
if ($conn->query($sql) === TRUE) {
    echo "Nouvelle contrainte de clé étrangère ajoutée avec succès pour la table competence.";
} else {
    echo "Erreur lors de l'ajout de la nouvelle contrainte de clé étrangère pour la table competence : " . $conn->error;
}
// Votre code existant pour la connexion à la base de données

// Code pour créer la liaison entre la table education et utilisateur
$sql = "ALTER TABLE education ADD CONSTRAINT fk_education_utilisateur FOREIGN KEY (id_utilisateur) REFERENCES utilisateur(id)";
if ($conn->query($sql) === TRUE) {
    echo "Contrainte de clé étrangère ajoutée avec succès pour la table education.";
} else {
    echo "Erreur lors de l'ajout de la contrainte de clé étrangère pour la table education : " . $conn->error;
}

// Code pour créer la liaison entre la table competence et utilisateur
$sql = "ALTER TABLE competence ADD CONSTRAINT fk_competence_utilisateur FOREIGN KEY (id_utilisateur) REFERENCES utilisateur(id)";
if ($conn->query($sql) === TRUE) {
    echo "Contrainte de clé étrangère ajoutée avec succès pour la table competence.";
} else {
    echo "Erreur lors de l'ajout de la contrainte de clé étrangère pour la table competence : " . $conn->error;
}


?>