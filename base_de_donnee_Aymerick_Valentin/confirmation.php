<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
   
    <title>Document</title>
</head>
<body>
<h1>Confirmation</h1>
    <p>Vos données ont été enregistrées avec succès.</p>
    <?php

include_once 'connexion.php';

    // Exemple de récupération de données depuis une table "utilisateurs"
    $query = "SELECT * FROM utilisateur";
    $result = mysqli_query($conn, $query);

    if ($result) {
        $rows = mysqli_fetch_all($result, MYSQLI_ASSOC);

        // Parcourir les résultats
        foreach ($rows as $row) {
            echo "ID: " . $row['id'] . "<br>";
            echo "Nom: " . $row['nom'] . "<br>";
            echo "Prénom: " . $row['prenom'] . "<br>";
            // Affichez d'autres colonnes ou traitez les données selon vos besoins
            echo "<br>";
        }

        mysqli_free_result($result);
    } else {
        echo "Erreur lors de la récupération des données : " . mysqli_error($conn);
    }

    $query = "SELECT * FROM education";
    $result = mysqli_query($conn, $query);

    if ($result) {
        $rows = mysqli_fetch_all($result, MYSQLI_ASSOC);

        // Parcourir les résultats
        foreach ($rows as $row) {
            echo "Etablissement: " . $row['etablissement'] . "<br>";
            echo "Diplôme: " . $row['diplome'] . "<br>";
            // Affichez d'autres colonnes ou traitez les données selon vos besoins
            echo "<br>";
        }

        mysqli_free_result($result);
    } else {
        echo "Erreur lors de la récupération des données de la table 'education' : " . mysqli_error($conn);
    }

    $query = "SELECT * FROM competence";
    $result = mysqli_query($conn, $query);

    if ($result) {
        $rows = mysqli_fetch_all($result, MYSQLI_ASSOC);

        // Parcourir les résultats
        foreach ($rows as $row) {
            echo "Compétence: " . $row['competence'] . "<br>";
            // Affichez d'autres colonnes ou traitez les données selon vos besoins
            echo "<br>";
        }

        mysqli_free_result($result);
    } else {
        echo "Erreur lors de la récupération des données de la table 'competence' : " . mysqli_error($conn);
    }

    $conn->close();

?>

</body>
</html>
