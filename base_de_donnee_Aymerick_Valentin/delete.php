<?php
require 'connexion.php';

// Vérifier si l'ID de l'enregistrement à supprimer est présent dans les paramètres de la requête
if (isset($_GET['id'])) {
    $id = $_GET['id'];

    // Requête pour supprimer l'enregistrement de la base de données
    $sql = "DELETE FROM utilisateur WHERE id = ?";
    $stmt = mysqli_prepare($conn, $sql);
    
    // Lier le paramètre ID à la requête préparée
    mysqli_stmt_bind_param($stmt, "i", $id);

    // Exécuter la requête préparée
    $result = mysqli_stmt_execute($stmt);

    if ($result) {
        echo 'L\'enregistrement a été supprimé avec succès.';

                // Rediriger vers la page table.php après la suppression
                header('Location: table.php');
                exit();
    } else {
        echo 'Erreur lors de la suppression de l\'enregistrement : ' . mysqli_error($conn);
    }
} else {
    echo 'ID de l\'enregistrement non spécifié.';
}

// Fermer la requête préparée et la connexion à la base de données
// mysqli_stmt_close($stmt);
// mysqli_close($conn);
