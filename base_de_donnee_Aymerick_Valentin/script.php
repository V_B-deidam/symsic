<?php

require('POO/utilisateur.php');
require('POO/education.php');
require('POO/competence.php');
// include 'table.php';

include_once 'connexion.php';

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


$nom =  $_GET['nom'];
$prenom = $_GET['prenom'];
$email = $_GET['email'];
$motDePasse = $_GET['motDePasse'];
$dateNaissance = $_GET['dateNaissance'];
$genre = $_GET['genre'];
$adresse = $_GET['adresse'];
$telephone = $_GET['telephone'];

$etablissement = $_GET['etablissement'];
$diplome = $_GET['diplome'];
$domaine = $_GET['domaine'];
$dateDebut = $_GET['date_debut'];
$dateFin = $_GET['date_fin'];
$enCours = isset($_GET['en_cours']) ? 1 : 0;

$competence = $_GET['competence'];

$utilisateur = new Utilisateur($nom, $prenom, $email, $motDePasse, $dateNaissance, $genre, $adresse, $telephone);
$utilisateur->enregistrerUtilisateur($conn);

$education = new Education($etablissement, $diplome, $domaine, $dateDebut, $dateFin, $enCours);
// Enregistrement de l'éducation
$education->enregistrerEducation($conn);


$competence = new Competence($competence);
// Enregistrement des compétences
$competence->enregistrerCompetence($conn);

// Supposons que l'objet Competence a une propriété "nom"
$competenceNom =$competence->competenceNom(); // Extraire le nom de l'objet Competence

// Utiliser $competenceNom dans votre requête SQL
$query = "INSERT INTO competence (nom) VALUES ('$competenceNom')";


if (!$resultCompetence) {
    echo "Erreur lors de l'insertion de la compétence : " . $conn->error;
} else {
    echo "La compétence a été insérée avec succès.";
}

$sqlEducation = "INSERT INTO education (etablissement, diplome, domaine, date_debut, date_fin, en_cours) VALUES ('$etablissement', '$diplome', '$domaine', '$dateDebut', '$dateFin', '$enCours')";

$resultEducation = $conn->query($sqlEducation);

if (!$resultEducation) {
    echo "Erreur lors de l'insertion de l'éducation : " . $conn->error;
} else {
    echo "L'éducation a été insérée avec succès.";
}

$conn->close();
header("Location: table.php");
exit();
