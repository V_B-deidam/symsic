<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Document</title>
</head>

<body>
    <form action="recherche.php" method="GET">
        <input type="text" name="query" placeholder="Entrez votre recherche">
        <button type="submit">Rechercher</button>
    </form>

    <?php

include_once 'connexion.php';

$search_query = $_GET['query'] ?? '';

if (!empty($search_query)) {
    $search_query = mysqli_real_escape_string($conn, $search_query);
    $sql = "SELECT * FROM utilisateur WHERE nom LIKE '%$search_query%' OR prenom LIKE '%$search_query%' OR email LIKE '%$search_query%' OR adresse LIKE '%$search_query%'";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            echo "Nom : " . $row['nom'] . "<br>";
            echo "Prénom : " . $row['prenom'] . "<br>";
            echo "adresse : " . $row['adresse'] . "<br>";
            echo "email : " . $row['email'] . "<br>";
            echo "<br>";
        }
    } else {
        echo "Aucun résultat trouvé.";
    }
} else {
    echo "Veuillez entrer un terme de recherche.";
}

// $queryEducation = $_GET['query_education'];
// $sqlEducation = "SELECT * FROM education WHERE column_education LIKE '%{$queryEducation}%'";
// $resultEducation = $conn->query($sqlEducation);

// while ($rowEducation = $resultEducation->fetch_assoc()) {
//     // Traitement des résultats de l'éducation
// }

// $queryCompetence = $_GET['query_competence'];
// $sqlCompetence = "SELECT * FROM competence WHERE column_competence LIKE '%{$queryCompetence}%'";
// $resultCompetence = $conn->query($sqlCompetence);

// while ($rowCompetence = $resultCompetence->fetch_assoc()) {
//     // Traitement des résultats de la compétence
// }


    $apiKey = "928698d6f31a2d7cd1b41b4fc785428c";
    $city = "Lille"; // Ville pour laquelle vous souhaitez obtenir la météo

    $url = "http://api.openweathermap.org/data/2.5/weather?q=" . urlencode($city) . "&appid=" . $apiKey;
    
    $response = file_get_contents($url);
    
    if ($response) {
        $weatherData = json_decode($response, true);
    
        // Affichage des données météorologiques
        $temperature = $weatherData['main']['temp'];
        $weatherDescription = $weatherData['weather'][0]['description'];
        $iconCode = $weatherData['weather'][0]['icon'];
    
        echo "<h1>Météo à $city</h1>";
        echo "<p>Température : " . round($temperature - 273.15) . "°C</p>";
        echo "<p>Description : $weatherDescription</p>";
        echo "<img src='http://openweathermap.org/img/w/$iconCode.png' alt='Weather Icon' height=250>";
    } else {
        echo "Impossible de récupérer les données météorologiques pour la ville de $city.";
    }
    ?>


</body>

</html>