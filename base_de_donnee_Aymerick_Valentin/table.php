<?php
require 'connexion.php';
require 'update.php';
require 'delete.php';

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$dom = new DOMDocument();

$table = $dom->createElement('table');
$table->setAttribute("border", 3);

$trHeader = $dom->createElement('tr');

$thColonne1 = $dom->createElement('th', 'id');
$thColonne2 = $dom->createElement('th', 'nom');
$thColonne3 = $dom->createElement('th', 'prenom');
$thColonne4 = $dom->createElement('th', 'email');
$thColonne5 = $dom->createElement('th', 'mot_de_passe');
$thColonne6 = $dom->createElement('th', 'genre');
$thColonne7 = $dom->createElement('th', 'adresse');
$thColonne8 = $dom->createElement('th', 'telephone');
$thColonne9 = $dom->createElement('th', 'etablissement');
$thColonne10 = $dom->createElement('th', 'diplome');
$thColonne11 = $dom->createElement('th', 'domaine');
$thColonne12 = $dom->createElement('th', 'date_debut');
$thColonne13 = $dom->createElement('th', 'date_fin');
$thColonne14 = $dom->createElement('th', 'en_cours');
$thColonne15 = $dom->createElement('th', 'competence');

$trHeader->appendChild($thColonne1);
$trHeader->appendChild($thColonne2);
$trHeader->appendChild($thColonne3);
$trHeader->appendChild($thColonne4);
$trHeader->appendChild($thColonne5);
$trHeader->appendChild($thColonne6);
$trHeader->appendChild($thColonne7);
$trHeader->appendChild($thColonne8);
$trHeader->appendChild($thColonne9);
$trHeader->appendChild($thColonne10);
$trHeader->appendChild($thColonne11);
$trHeader->appendChild($thColonne12);
$trHeader->appendChild($thColonne13);
$trHeader->appendChild($thColonne14);
$trHeader->appendChild($thColonne15);

$table->appendChild($trHeader);

$sql = "SELECT utilisateur.id, utilisateur.nom, utilisateur.prenom, utilisateur.email, utilisateur.mot_de_passe, utilisateur.genre, utilisateur.adresse, utilisateur.telephone, COALESCE(education.etablissement, '') AS etablissement, COALESCE(education.diplome, '') AS diplome, COALESCE(education.domaine, '') AS domaine, COALESCE(education.date_debut, '') AS date_debut, COALESCE(education.date_fin, '') AS date_fin, COALESCE(education.en_cours, '') AS en_cours, COALESCE(competence.competence, '') AS competence, utilisateur.id AS id_utilisateur
FROM utilisateur
LEFT JOIN education ON utilisateur.id = education.id_utilisateur
LEFT JOIN competence ON utilisateur.id = competence.id_utilisateur";

$result = mysqli_query($conn, $sql);

if ($result) {

    if (mysqli_num_rows($result) > 0) {

        while ($row = mysqli_fetch_assoc($result)) {

            $trData = $dom->createElement('tr');

            $tdColonne1 = $dom->createElement('td', $row['id']);
            $tdColonne2 = $dom->createElement('td', $row['nom']);
            $tdColonne3 = $dom->createElement('td', $row['prenom']);
            $tdColonne4 = $dom->createElement('td', $row['email']);
            $tdColonne5 = $dom->createElement('td', $row['mot_de_passe']);
            $tdColonne6 = $dom->createElement('td', $row['genre']);
            $tdColonne7 = $dom->createElement('td', $row['adresse']);
            $tdColonne8 = $dom->createElement('td', $row['telephone']);
            $tdColonne9 = $dom->createElement('td', $row['etablissement']);
            $tdColonne10 = $dom->createElement('td', $row['diplome']);
            $tdColonne11 = $dom->createElement('td', $row['domaine']);
            $tdColonne12 = $dom->createElement('td', $row['date_debut']);
            $tdColonne13 = $dom->createElement('td', $row['date_fin']);
            $tdColonne14 = $dom->createElement('td', $row['en_cours']);
            $tdColonne15 = $dom->createElement('td', $row['competence']);
            

            // Ajouter les cellules de données à la ligne de données
            $trData->appendChild($tdColonne1);
            $trData->appendChild($tdColonne2);
            $trData->appendChild($tdColonne3);
            $trData->appendChild($tdColonne4);
            $trData->appendChild($tdColonne5);
            $trData->appendChild($tdColonne6);
            $trData->appendChild($tdColonne7);
            $trData->appendChild($tdColonne8);
            $trData->appendChild($tdColonne9);
            $trData->appendChild($tdColonne10);
            $trData->appendChild($tdColonne11);
            $trData->appendChild($tdColonne12);
            $trData->appendChild($tdColonne13);
            $trData->appendChild($tdColonne14);
            $trData->appendChild($tdColonne15);

            $table->appendChild($trData);

            $tdActions = $dom->createElement('td');
            $btnUpdate = $dom->createElement('button', 'Modifier');

            $tdAction = $dom->createElement('td');
            $btnDelete = $dom->createElement('button', 'Supprimer');

            $btnUpdate->setAttribute('onclick', "window.location.href = 'update.php?id=" . $row['id'] . "'");
            $btnDelete->setAttribute('onclick', "window.location.href = 'delete.php?id=" . $row['id'] . "'");

            $tdActions->appendChild($btnUpdate);
            $tdAction->appendChild($btnDelete); 

            $trData->appendChild($tdActions);
            $trData->appendChild($tdAction);
        }
  $dom->appendChild($table);
    } else {
        echo 'Aucun enregistrement trouvé.';
    }
} else {
    echo 'Erreur de requête : ' . mysqli_error($conn);
}

$xmlString = $dom->saveXML();
echo $xmlString;
var_dump($xmlString);
// mysqli_close($conn);
?>
