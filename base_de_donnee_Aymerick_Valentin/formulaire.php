<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Document</title>
</head>

<body>

    <form action="script.php" method="GET" id="container">
        <div>Formulaire d'enregistrement</div>
        <label for="nom">Nom :</label>
        <input type="text" name="nom" id="nom" required>
        <br>

        <label for="prenom">Prénom :</label>
        <input type="text" name="prenom" id="prenom" required>
        <br>

        <label for="email">Email :</label>
        <input type="email" name="email" id="email" required>
        <br>

        <label for="motDePasse">Mot de passe :</label>
        <input type="password" name="motDePasse" id="motDePasse" required>
        <br>

        <label for="dateNaissance">Date de naissance :</label>
        <input type="date" name="dateNaissance" id="dateNaissance" required>
        <br>

        <label for="genre">Genre :</label>
        <select name="genre" id="genre" required>
            <option value="homme">Homme</option>
            <option value="femme">Femme</option>
            <option value="femme">autre</option>
        </select>
        <br>

        <label for="adresse">adresse</label>
        <input type="text" name="adresse" id="adresse" required>
        <br>

        <label for="telephone">Téléphone :</label>
        <input type="tel" name="telephone" id="telephone" required>
        <br>

        <div>Informations sur l'éducation</div>

        <label for="etablissement">Nom de l'établissement :</label>
        <input type="text" name="etablissement" id="etablissement" required>
        <br>

        <label for="diplome">Diplôme :</label>
        <input type="text" name="diplome" id="diplome" required>
        <br>

        <label for="domaine">Domaine :</label>
        <input type="text" name="domaine" id="domaine" required>
        <br>

        <label for="date_debut">Date de début :</label>
        <input type="date" name="date_debut" id="date_debut" required>
        <br>

        <label for="date_fin">Date de fin :</label>
        <input type="date" name="date_fin" id="date_fin" required>
        <br>

        <label for="en_cours">En cours :</label>
        <input type="checkbox" name="en_cours" id="en_cours">
        <br>

        <div>Compétences</div>

        <label for="competence">Compétence :</label>
        <textarea id="competence" name="competence" rows="4" cols="40"></textarea>

        <br>

        <input type="submit" value="Enregistrer">
    </form>
</body>

</html>