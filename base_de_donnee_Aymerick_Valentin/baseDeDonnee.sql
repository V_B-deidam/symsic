CREATE TABLE utilisateur (
  id INT PRIMARY KEY AUTO_INCREMENT,
  nom VARCHAR(50),
  prenom VARCHAR(50),
  email VARCHAR(100),
  mot_de_passe VARCHAR(100),
  date_de_naissance DATE,
  genre ENUM('Homme', 'Femme', 'Autre'),
  adresse VARCHAR(255),
  telephone VARCHAR(20)
);


CREATE TABLE education (
  id INT PRIMARY KEY AUTO_INCREMENT,
  id_utilisateur INT,
  etablissement VARCHAR(100),
  diplome VARCHAR(100),
  domaine VARCHAR(100),
  date_debut DATE,
  date_fin DATE,
  en_cours BOOLEAN,
  FOREIGN KEY (id_utilisateur) REFERENCES utilisateur(id)
);

CREATE TABLE competence (
  id INT PRIMARY KEY AUTO_INCREMENT,
  id_utilisateur INT,
  competence VARCHAR(100),
  FOREIGN KEY (id_utilisateur) REFERENCES utilisateur(id)
);
