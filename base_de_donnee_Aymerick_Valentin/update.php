<?php
require 'connexion.php';

if (isset($_POST['id'])) {
    // Récupérer l'ID de l'enregistrement à modifier
    // $id = $_POST['id'];
}

// Vérifier si le formulaire a été soumis
if (isset($_POST['id']) && isset($_POST['nouveau_nom']) && isset($_POST['nouveau_prenom']) && isset($_POST['nouveau_email']) && isset($_POST['nouveau_mot_de_passe']) && isset($_POST['nouveau_genre']) && isset($_POST['nouveau_adresse']) && isset($_POST['nouveau_telephone'])) {
    // Récupérer les valeurs du formulaire
    $id = $_POST['id'];
    $nouveauNom = $_POST['nouveau_nom'];
    $nouveauPrenom = $_POST['nouveau_prenom'];
    $nouveauEmail = $_POST['nouveau_email'];
    $nouveauMotDePasse = $_POST['nouveau_mot_de_passe'];
    $nouveauGenre = $_POST['nouveau_genre'];
    $nouveauAdresse = $_POST['nouveau_adresse'];
    $nouveauTelephone = $_POST['nouveau_telephone'];

    // Vérifier si les valeurs nécessaires sont définies
    if ($id && $nouveauNom && $nouveauPrenom && $nouveauEmail && $nouveauMotDePasse && $nouveauGenre && $nouveauAdresse && $nouveauTelephone) {
        // Requête de mise à jour avec des paramètres liés
        $sql = "UPDATE utilisateur SET nom = ?, prenom = ?, email = ?, mot_de_passe = ? , genre = ?, adresse = ?, telephone = ? WHERE id = ?";
        $stmt = mysqli_prepare($conn, $sql);

        // Vérifier si la préparation de la requête a réussi
        if ($stmt) {
            // Lier les valeurs des paramètres
            mysqli_stmt_bind_param($stmt, 'sssssssi', $nouveauNom, $nouveauPrenom, $nouveauEmail, $nouveauMotDePasse, $nouveauGenre, $nouveauAdresse, $nouveauTelephone, $id);

            // Exécuter la requête
            if (mysqli_stmt_execute($stmt)) {
                echo "Mise à jour effectuée avec succès.";
                header('Location: table.php');
                exit();
            } else {
                echo "Erreur de mise à jour : " . mysqli_error($conn);
            }

            // Fermer le statement
            // mysqli_stmt_close($stmt);
        } else {
            echo "Erreur de préparation de la requête : " . mysqli_error($conn);
        }
    } else {
        echo "Valeurs manquantes pour la mise à jour.";
    }
}

// Requête pour récupérer les données de l'enregistrement à modifier
if (isset($id)) {
    $sql = "SELECT * FROM utilisateur WHERE id = $id";
    $result = mysqli_query($conn, $sql);

    if ($result) {
        // Vérifier s'il y a un enregistrement
        if (mysqli_num_rows($result) > 0) {
            $row = mysqli_fetch_assoc($result);
            ?>
            <form method="POST" action="update.php">
                <input type="hidden" name="id" value="<?php echo $row['id']; ?>">
                <label>Nouveau nom :</label>
                <input type="text" name="nouveau_nom" value="<?php echo $row['nom']; ?>"><br>
                <label>Nouveau prénom :</label>
                <input type="text" name="nouveau_prenom" value="<?php echo $row['prenom']; ?>"> <br>
                <label>Nouveau email :</label>
                <input type="text" name="nouveau_email" value="<?php echo $row['email']; ?>"><br>
                <label>Nouveau mot de passe :</label>
                <input type="text" name="nouveau_mot_de_passe" value="<?php echo $row['mot_de_passe']; ?>"><br>
                <label>Nouveau genre :</label>
                <input type="text" name="nouveau_genre" value="<?php echo $row['genre']; ?>"><br>
                <label>Nouvelle adresse :</label>
                <input type="text" name="nouveau_adresse" value="<?php echo $row['adresse']; ?>"><br>
                <label>Nouveau telephone :</label>
                <input type="text" name="nouveau_telephone" value="<?php echo $row['telephone']; ?>"><br>
                <input type="submit" value="Modifier">
            </form>
            <?php
        } else {
            echo "Enregistrement non trouvé.";
        }
    } else {
        echo 'Erreur de requête : ' . mysqli_error($conn);
    }
} else {
    echo "ID de l'enregistrement manquant.";
}

// Fermer la connexion à la base de données
// mysqli_close($conn);
?>
